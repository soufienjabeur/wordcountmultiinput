Le programme WordCountMultiInput est une évolution de l'exemple simple WorCount. 
Il permet de calculer le nombre d'un mot spécifique dans différents fichiers donnés sous différents répertoires, en décomposant le calcul en deux étapes:



*  L'étape de Mapping, qui permet de découper le texte en mots, de comparer chaque mot avec le mot spécifié et de délivrer en sortie un flux textuel, où chaque ligne contient le mot trouvé,
suivi de la valeur 1 (pour dire que le mot a été trouvé une fois)



*  L'étape de Reducing, qui permet de faire la somme des 1 pour chaque mot, pour trouver le nombre total d'occurrences de ce mot dans le texte.


Pour implémenter ce programme, on se servit de maven qui permet parfaitement les dépendances.
Pour plus de détails sur l'installation, la configuration de Hadoop et l'implémentation de cet exemple de, vous pouvez consulter cet article http://sysblog.informatique.univ-paris-diderot.fr/?p=1861&preview=true