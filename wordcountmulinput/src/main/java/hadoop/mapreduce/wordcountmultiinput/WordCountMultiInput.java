package hadoop.mapreduce.wordcountmultiinput;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCountMultiInput {
    public static void main(String[] args) throws Exception {
        // Create a job by providing the configuration and a text description of the task
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "word count multi-input");

        // We specify the classes WordCount, Map, Combine and Reduce
        job.setJarByClass(WordCountMultiInput.class);
        job.setMapperClass(TokenizerMapperMultiInput.class);
        job.setCombinerClass(IntSumReducerMultiInput.class);
        job.setReducerClass(IntSumReducerMultiInput.class);

        //We precise there are multi-path, not a single path
        Path inputFilePath = new Path(args[0]+"/*/*");
        Path outputFilePath = new Path(args[1]);

        // Definition of the key / value types of our problem
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // Definition of input and output files (here considered as arguments to be specified during execution)

        //Using multiple input file
        MultipleInputs.addInputPath(job, inputFilePath, TextInputFormat.class, TokenizerMapperMultiInput.class);
        FileOutputFormat.setOutputPath(job, outputFilePath);

        // Delete the output file if it already exists
        FileSystem fs = FileSystem.newInstance(conf);
        if (fs.exists(outputFilePath)) {
            fs.delete(outputFilePath, true);
        }

        // run the job
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
